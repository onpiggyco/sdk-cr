<?php

/**
 * CR SDK
 * 
 * @author Guilherme Macêdo
 * @version 1.0
 * 
 * @copyright 2012, Cupom Rápido
 */
class CR {
  /**
   * API URL
   * 
   * @var string
   */

  CONST API_URL = 'http://cupomrapido.com.br/api';

  /**
   * Status Valid(Válido in brazilian portuguese)
   * 
   * @var int
   */
  CONST COUPON_STATUS_VALIDO = 1;

  /**
   * Call Cupom Rápido API
   * 
   * @access private
   * @param string $endpoint Endpoint to call
   * 
   * @return array|false 
   */
  private function api($endpoint) {
    //call api
    $ch = curl_init(self::API_URL . $endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);

    //decode JSON result and return
    return json_decode($result, true);
  }

  /**
   * Validate coupon
   * 
   * @param string $store_token Store's token at Cupom Rápido
   * @param string $code Coupon code to validate
   * 
   * @return bool|array
   */
  public function validate($store_token, $code) {
    //call api
    $result = $this->api("/coupon/validate?token=" . $store_token . "&code=" . $code);

    //check if coupon is found and if is valid
    if (count($result) > 0) {
      if ($result['status'] == CR::COUPON_STATUS_VALIDO) {
        return $result;
      }
    }

    return false;
  }

  /**
   * Use coupon
   * 
   * @param string $coupon_token Coupon's Token
   * @param int $coupon_id Coupon's Id
   * 
   * @return bool True if operation is succesful, false otherwise
   */
  public function usar($coupon_token, $coupon_id) {
    $result = $this->api("/coupon/use?token=" . $coupon_token . "&id=" . $coupon_id);

    return $result['success'];
  }

}
